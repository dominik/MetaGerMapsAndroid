package de.suma_ev.dominik.metagermaps;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.koushikdutta.async.http.server.AsyncHttpServer;
import com.koushikdutta.async.http.server.AsyncHttpServerRequest;
import com.koushikdutta.async.http.server.AsyncHttpServerResponse;
import com.koushikdutta.async.http.server.HttpServerRequestCallback;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import de.suma_ev.dominik.metagermaps.tileDataBase.AppDatabase;
import de.suma_ev.dominik.metagermaps.tileDataBase.Tile;

class TileServer {

    public TileServer(final AppDatabase db){



        AsyncHttpServer server = new AsyncHttpServer();
        server.get("/", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                int z = Integer.parseInt(request.getQuery().getString("z"));
                int x = Integer.parseInt(request.getQuery().getString("x"));
                int y = Integer.parseInt(request.getQuery().getString("y"));

                // Lets check if the tile is stored in the db
                Tile tile = db.tileDao().getTile(x, y, z);
                if(tile != null){
                    response.code(200);
                    response.send("image/png", tile.getImage());
                }else {

                    String urlString = "https://maps.metager.de/tile_cache/" + z + "/" + x + "/" + y + ".png";
                    try (InputStream in = (new URL(urlString).openStream());) {
                        byte[] image = IOUtils.toByteArray(in);

                        tile = new Tile(x, y, z, image, new Date());
                        db.tileDao().insertTile(tile);

                        response.code(200);
                        response.send("image/png", image);
                    } catch (IOException e) {
                        response.code(404);
                        response.send("File not found!");
                    }
                }


            }
        });
        server.listen(63825);
    }
}
