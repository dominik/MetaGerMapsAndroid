package de.suma_ev.dominik.metagermaps;

import android.webkit.JavascriptInterface;

import java.lang.annotation.Annotation;

class JavaScriptInterface {
    @JavascriptInterface
    public int getVersionCode(){
        return BuildConfig.VERSION_CODE;
    }
}
