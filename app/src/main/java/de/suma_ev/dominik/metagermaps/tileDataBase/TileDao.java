package de.suma_ev.dominik.metagermaps.tileDataBase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface TileDao {

    @Query("SELECT * FROM tile")
    List<Tile> getAll();

    @Query("SELECT * FROM tile WHERE x = :x AND y = :y AND z = :z LIMIT 1")
    Tile getTile(int x, int y, int z);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTile(Tile tile);
}
