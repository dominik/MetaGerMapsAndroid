package de.suma_ev.dominik.metagermaps;

import android.Manifest;
import android.arch.persistence.room.Room;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import de.suma_ev.dominik.metagermaps.tileDataBase.AppDatabase;

public class MapsView extends AppCompatActivity {

    private WebView webview;

    private final int PERMISSION_REQUEST_LOCATION = 1;
    private AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_view);

        // Check Permission for Location
        this.checkPermissions();

        this.db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "tiledb").build();
        new TileServer(this.db);

        loadMaps();

    }

    private void checkPermissions() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED){
            // Request the permission
            String[] permission = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(this, permission, PERMISSION_REQUEST_LOCATION);
        }else{
            this.enableGPS();
        }
    }

    private void enableGPS() {
    }

    private void loadMaps() {
        this.webview = findViewById(R.id.webview);

        WebChromeClient chromeClient = new WebChromeClient(){
            @Override
            public void onGeolocationPermissionsShowPrompt(String origin,
                                                           GeolocationPermissions.Callback callback) {
                // Always grant permission since the app itself requires location
                // permission and the user has therefore already granted it
                callback.invoke(origin, true, false);
            }
        };

        webview.setWebViewClient(new WebViewClient());
        webview.setWebChromeClient(chromeClient);
        webview.addJavascriptInterface(new JavaScriptInterface(), "android");

        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setAppCacheEnabled(true);
        settings.setGeolocationEnabled(true);

        webview.setWebContentsDebuggingEnabled(true);

        webview.clearCache(true);
        webview.clearHistory();

        webview.loadUrl("https://maps.metager.de");

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        switch (requestCode) {
            case PERMISSION_REQUEST_LOCATION: {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    this.enableGPS();
                }
                return;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.db.close();
    }
}
