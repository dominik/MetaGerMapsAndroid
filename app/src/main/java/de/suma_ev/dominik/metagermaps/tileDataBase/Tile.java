package de.suma_ev.dominik.metagermaps.tileDataBase;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity(primaryKeys = {"x", "y", "z"})
public class Tile {

    private int x;

    private int y;

    private int z;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] image;

    @ColumnInfo(name = "last_modified")
    private Date lastModified;

    @ColumnInfo(name = "last_used")
    private Date lastUsed;

    public Tile(int x, int y, int z, byte[] image, Date lastModified){
        this.x = x;
        this.y = y;
        this.z = z;
        this.image = image;
        this.lastModified = lastModified;
        this.lastUsed = lastModified;
    }

    public int getX(){
        return x;
    }
    public void setX(int x){
        this.x = x;
    }

    public int getY(){
        return y;
    }
    public void setY(int y){
        this.y = y;
    }

    public int getZ(){
        return z;
    }
    public void setZ(int z){
        this.z = z;
    }

    public byte[] getImage(){
        return image;
    }
    public void setImage(byte[] image){
        this.image = image;
    }

    public Date getLastModified(){
        return lastModified;
    }
    public void setLastModified(Date lastModified){
        this.lastModified = lastModified;
    }

    public Date getLastUsed(){
        return this.lastUsed;
    }
    public void setLastUsed(Date lastUsed){
        this.lastUsed = lastUsed;
    }
}
